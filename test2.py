import tensorflow as tf
from tensorflow.keras.applications.vgg16 import VGG16
from keras.applications.inception_v3 import InceptionV3

vgg = VGG16(include_top=False, input_shape=(48, 48, 3), pooling='avg', weights='imagenet')

inc = InceptionV3(include_top=False, input_shape=(75, 75, 3), pooling='avg', weights='imagenet')

print(vgg.summary())
print(inc.summary())